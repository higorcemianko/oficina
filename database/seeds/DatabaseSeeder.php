<?php

use Illuminate\Database\Seeder;
use oficina\Marca;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(MarcaTableSeeder::class);
    }
}

class MarcaTableSeeder extends Seeder{
	public function run(){
        Marca::create(['descricao' => 'Volkswagem']);
        Marca::create(['descricao' => 'Fiat']);
        Marca::create(['descricao' => 'Chevrolet']);
        Marca::create(['descricao' => 'Hyunday']);
    } 
}
	