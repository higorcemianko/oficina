<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <!-- Latest compiled and minified JavaScript -->

    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    
    <title>{{ config('name', 'Ordens de Serviço - Oficina') }}</title>


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style type="text/css">
        body {
            background-color: #DCDCDC;
            font-family: tahoma;
        }

        
        nav.navbar {
            background-color: #808080;
            color: black;
        }

        ul.dropdown-menu {
            background-color: #C0C0C0;
            color: black;   
        }

        li.dropdown a.dropdown-toggle {
            color: black;   
        }

        li.dropdown a.dropdown-toggle:hover:not(.active) {
            background-color: #D3D3D3;
        }

        ul.dropdown-menu:hover:not(.active) {
            background-color: #D3D3D3;
        }

        ul.navbar.navbar-nav.navbar-right {
            color: black;   
        }       

        button.btn.btn-primary{
            background-color: #696969;
            border-color: #696969;

        }
        a.btn.btn-link{
            color: black;
        }

        a.btn.btn-primary{
            background-color: #696969;
            border-color: #696969; 
        }

       
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" >
            <div class="container" >
                <div class="navbar-header" >

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <font color="black" ><b>{{ config('name', 'Ordens de Serviço - Oficina') }}</b></font>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}" style="color: black">Login</a></li>
                            <li><a href="{{ route('register') }}" style="color: black">Cadastrar-se</a></li>
                        @else
                            <li class="dropdown">                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Clientes <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{action('ClienteController@lista')}}">Todos os clientes</a>
                                        <a href="{{action('ClienteController@novo')}}">Novo cliente</a>
                                    </li>
                                </ul>   
                            </li>
                            <li class="dropdown">                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Veículos <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{action('VeiculoController@lista')}}">Todos os veículos</a>
                                        <a href="{{action('VeiculoController@novo')}}">Novo veículo</a>
                                    </li>
                                </ul>   
                            </li> 
                            <li class="dropdown">                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Itens <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{action('ItemController@lista')}}">Todos os itens</a>
                                        <a href="{{action('ItemController@novo')}}">Novo item</a>
                                    </li>
                                </ul>   
                            </li>
                            <li class="dropdown">                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Serviços <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{action('ServicoController@lista')}}">Todos os serviços</a>
                                        <a href="{{action('ServicoController@novo')}}">Novo serviço</a>
                                    </li>
                                </ul>   
                            </li>  
                            <li class="dropdown">                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Ordens <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{action('OrdemController@lista')}}">Todas as ordens</a>
                                        <a href="{{action('OrdemController@nova')}}">Nova ordem</a>
                                    </li>
                                </ul>   
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>        
                        @endif
                        
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    


    
    
</body>
</html>
