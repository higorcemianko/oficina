    function validarCliente(cpfcnpj, tipopessoa) {
 
        //cnpj = cnpj.replace(/[^\d]+/g,'');
        if(tipopessoa == 2){
            msg = "Informe um CNPJ válido!";
            if(cpfcnpj== ''){
                alert(msg);
                return false;
            } 
            
             
            if (cpfcnpj.length != 14){
                alert(msg);
                return false;
            }
                
         
            // Elimina CNPJs invalidos conhecidos
            if (cpfcnpj == "00000000000000" || 
                cpfcnpj == "11111111111111" || 
                cpfcnpj == "22222222222222" || 
                cpfcnpj == "33333333333333" || 
                cpfcnpj == "44444444444444" || 
                cpfcnpj == "55555555555555" || 
                cpfcnpj == "66666666666666" || 
                cpfcnpj == "77777777777777" || 
                cpfcnpj == "88888888888888" || 
                cpfcnpj == "99999999999999"){
                alert(msg);
                return false;
            }
                
                 
            // Valida DVs
            tamanho = cpfcnpj.length - 2
            numeros = cpfcnpj.substring(0,tamanho);
            digitos = cpfcnpj.substring(tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--) {
              soma += numeros.charAt(tamanho - i) * pos--;
              if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0)){
                alert(msg);
                return false;
            }
                
                 
            tamanho = tamanho + 1;
            numeros = cpfcnpj.substring(0,tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--) {
              soma += numeros.charAt(tamanho - i) * pos--;
              if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1)){
                alert(msg);
                return false;
            }

        }
            
        else{
            msg = "Informe um CPF válido!";
            var Soma;
            var Resto;
            Soma = 0;
            if (cpfcnpj == "00000000000"){
                alert(msg);                    
                return false;
            } 
                
            
            for (i=1; i<=9; i++) Soma = Soma + parseInt(cpfcnpj.substring(i-1, i)) * (11 - i);
            Resto = (Soma * 10) % 11;
            
            if ((Resto == 10) || (Resto == 11))  Resto = 0;
            if (Resto != parseInt(cpfcnpj.substring(9, 10)) ){
                alert(msg);
                return false;
            } 
            
            Soma = 0;
            for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpfcnpj.substring(i-1, i)) * (12 - i);
            Resto = (Soma * 10) % 11;
            
            if ((Resto == 10) || (Resto == 11))  Resto = 0;
            if (Resto != parseInt(cpfcnpj.substring(10, 11) ) ){
                alert(msg);                    
                return false;

            }
        }
                   
        return true;
    
    }