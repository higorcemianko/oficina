<?php

namespace oficina;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model{

	public $timestamps = false;
	protected $guarded = ['id'];

    public function produtos(){
    	return $this->hasMany('estoque\Veiculo');
	}
}
